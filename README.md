# Conky Blue Theme - README #
---

### Overview ###

This **Conky Blue** theme can be used to display different system information on your computer desktop. Therefore the [**Conky Application**](https://github.com/brndnmtthws/conky) (software package '**conky-all**') has to be installed and started with this **Conky Blue** configuration file. A PHP file called '**ip.php**' can be used to display the external IP address in the Conky window (optional) and has to be installed onto an external webserver (accessible from the web).

### Version ###

This **Conky Blue** theme (file: '**conky.conf**') is the complete version for large screens of 1920 x 1080 pixels or higher, but can also be adapted for use with smaller screens. This version is using the new [**Conky version 1.10 Syntax**](https://github.com/brndnmtthws/conky/wiki/Configuration-Settings) and will not run with older versions! So please update your Conky version before using this theme :-)

![Conky Blue Theme](development/readme/conky1.png "Conky Blue Theme")

### Setup ###

* Install the [**Conky Application**](https://github.com/brndnmtthws/conky) (software package '**conky-all**') on your computer.
* Install the additional software package '**lm-sensors**' to display the CPU temperature.
* Install the additional software package '**httpie**' to display the external IP address.
* Install the PHP script '**ip.php**' onto an external webserver (external IP address).
* Then enter the URL to the PHP script into the Conky configuration file (file: '**conky.conf**').
* Enable the '**window transparency**' in your desktop environment.
* (If the window transparency does not work, please check the paragraph '**Transparency**' below!)
* Copy this Conky configuration file to: **~/Conky/conky.conf**.
* Customize the Conky configuration to fit your needs.
* Start Conky with the command: **conky -p 4 -c ~/Conky/conky.conf &**.

### Transparency ###
Sometimes it's not easy to get a transparent Conky window. It depends on the used desktop environment, window manager and transparency settings. With the help of the following two additional software packages, this should be possible with nearly every desktop environment:

* **xcompmgr** - X composition manager.
* **x11-apps** - X applications.

Install the two packages and use the startscript '**start_conky.sh**' to start the Conky application and get a a transparent window. You can also use the stopscript '**stop_conky.sh**' to stop the Conky process(es). The manual commands to start Conky from the command line would be:

* **xcompmgr -c -t-5 -l-5 -r4.2 -o.55 &**
* **conky -p 4 -c ~/Conky/conky.conf &**
* **transset 1 -v -n "conky ($(hostname))"**

### Available Files ###

Basic files used for the '**normal**' installation:

* **conky.conf** - Conky configuration file and theme '**Conky Blue**'.
* **ip.php** - PHP script used to display the external IP address.

Additional files for using with the '**X composition manager**' (optional - if transparency does not work):

* **conky.desktop** - Desktop file (for Linux environments) to start Conky.
* **conky.png** - Conky logo, used for the desktop file.
* **start_conky.sh** - Bash script to start the Conky application.
* **stop_conky.sh** - Bash script to stop the Conky application.

### Additional Information ###

For more information about the Conky configuration options check the following links:

- **[https://github.com/brndnmtthws/conky](https://github.com/brndnmtthws/conky)**
- **[http://conky.sourceforge.net/docs.html](http://conky.sourceforge.net/docs.html)**
- **[http://ifxgroup.net/conky.htm](http://ifxgroup.net/conky.htm)**

### Support ###

This is a free configuration/template and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Conky Blue** theme is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
