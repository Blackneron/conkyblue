#!/bin/bash
clear
xcompmgr -c -t-5 -l-5 -r4.2 -o.55 &
conky -p 4 -c ~/Conky/conky.conf &
sleep 8
transset 1 -v -n "conky ($(hostname))"
