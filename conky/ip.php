<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//           PBexip - Show external IP Address and Country            //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.1 / 26.06.2017                      //
//                                                                    //
//                      Copyright 2017 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //


// #####################################################################
// #####################################################################
// ###########   C O N F I G U R A T I O N   -   B E G I N   ###########
// #####################################################################
// #####################################################################


// =====================================================================
// Please specify the default timezone.
// =====================================================================
//
// If the timezone is not defined in your PHP configuration file, you
// can specify it with the following setting:
//
//  date_default_timezone_set('UTC');
//
// You have to uncomment the setting and enter your preferred timezone.
// For a list of available timezones check the following link:
//
//   http://www.php.net/manual/en/timezones.php
//
// Normally it is easier to specify the default timezone in the PHP
// configuration file 'php.ini'. You have to add the following lines to
// the configuration file:
//
//   [Date]
//   date.timezone = UTC
//
// You then can replace 'UTC' with your own timezone.
//
// =====================================================================
date_default_timezone_set('Europe/Zurich');


// =====================================================================
// Please specify which service to use to get the information about the
// external IP address.
//
//   1 = myexternalip.com
//   2 = ifconfig.me
//
// =====================================================================
$ip_service = 1;


// =====================================================================
// Please specify which service to use to get the information about the
// location (name of the country).
//
//   1 = ip2country.info
//   2 = netip.de
//
// =====================================================================
$location_service = 1;


// =====================================================================
// Please specify the name of the IP cache file.
// =====================================================================
$cache_file_name = "ip";


// =====================================================================
// Please specify the cache expiration time.
//
//      60 =  1 minute
//     120 =  2 minutes
//     180 =  3 minutes
//     240 =  4 minutes
//     300 =  5 minutes
//     600 = 10 minutes
//     900 = 15 minutes
//    1800 = 30 minutes
//    2700 = 45 minutes
//    3600 =  1 hour
//   10800 =  3 hours
//   21600 =  6 hours
//   43200 = 12 hours
//   86400 = 24 hours
//
// =====================================================================
$cache_expiration = 300;


// #####################################################################
// #####################################################################
// #############   C O N F I G U R A T I O N   -   E N D   #############
// #####################################################################
// #####################################################################


// =====================================================================
// Check if the IP cache file exist.
// =====================================================================
if (file_exists($cache_file_name)) {


  // ===================================================================
  // Get the modification time of the cached file.
  // ===================================================================
  $cache_file_time = filemtime($cache_file_name);


  // ===================================================================
  // Specify the expiration time for the cached data.
  // ===================================================================
  $expiration_time = $cache_file_time + $cache_expiration;


  // ===================================================================
  // The cache file does not exist.
  // ===================================================================
} else {


  // ===================================================================
  // Set the expiration time to zero.
  // ===================================================================
  $expiration_time = 0;

} // The cache file does not exist.


// =====================================================================
// Specify the actual time for comparing.
// =====================================================================
$actual_time = time();


// =====================================================================
// Check if the cache file has to be renewed.
// =====================================================================
if ($actual_time > $expiration_time) {


  // ===================================================================
  // Check if service 1 should be used - External IP address.
  // ===================================================================
  if ($ip_service == 1) {


    // =================================================================
    // Get the external IP address - Service 1.
    // =================================================================
    $ip_address = trim(file_get_contents('http://myexternalip.com/raw'));


    // =================================================================
    // The service 2 should be used - External IP address.
    // =================================================================
  } else {


    // =================================================================
    // Get the external IP address - Service 2.
    // =================================================================
    $ip_address = @trim(file_get_contents('http://ifconfig.me/ip'));
  }


  // ===================================================================
  // Check if service 1 should be used - Geolocation.
  // ===================================================================
  if ($location_service == 1) {


    // =================================================================
    // Initialize the array for the location information.
    // =================================================================
    $location_array = array();


    // =================================================================
    // Get the location output for the actual IP address.
    // =================================================================
    $location_json = file_get_contents('https://api.ip2country.info/ip?'.$ip_address);


    // =================================================================
    // Encode the location output.
    // =================================================================
    $location_json = utf8_encode($location_json);


    // =================================================================
    // Decode the location output.
    // =================================================================
    $location_array = json_decode($location_json, true);


    // =================================================================
    // Get the coutry name out of the location output.
    // =================================================================
    $country = $location_array['countryName'];


    // =================================================================
    // The service 2 should be used - Geolocation.
    // =================================================================
  } else {


    // =================================================================
    // Get the location output for the actual IP address.
    // =================================================================
    $response = file_get_contents('http://www.netip.de/search?query='.$ip_address);


    // =================================================================
    // Get the coutry name out of the location output.
    // =================================================================
    $country = preg_match('#Country: [A-Z]{2} - (.*?)&nbsp;#i',$response,$value) && !empty($value[1]) ? $value[1] : "Unknown";
  }


  // ===================================================================
  // Specify the information.
  // ===================================================================
  $information = $ip_address." / ".$country;


  // ===================================================================
  // Search for an error message in the output.
  // ===================================================================
  $position = stripos($information, "404");


  // ===================================================================
  // Check if there is an error message in the output.
  // ===================================================================
  if ($position !== false) {


    // =================================================================
    // Specify the information.
    // =================================================================
    $information = "Network Error!";
  }


  // ===================================================================
  // Save the IP address and country name into a cache file.
  // ===================================================================
  file_put_contents($cache_file_name, $information);


  // ===================================================================
  // Specify the expiration time for the newely cached data.
  // ===================================================================
  $expiration_time = time() + $cache_expiration;


  // ===================================================================
  // The requested information has to be read from the cache file.
  // ===================================================================
} else {


  // ===================================================================
  // Read the information from the cache file.
  // ===================================================================
  $information = file_get_contents($cache_file_name);
}


// =====================================================================
// Calculate the number of seconds until the next refresh.
// =====================================================================
$refresh_seconds = $expiration_time - $actual_time;


// =====================================================================
// Display the information.
// =====================================================================
echo $information." (".$refresh_seconds."s)";
